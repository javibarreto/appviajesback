<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class Viaje extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Id'        => $this->id_viaje,
            'Codigo'          => $this->co_viaje,
            'NuPlazas'          => $this->nu_plazas,
            'Origen'          => $this->nb_origen,
            'Destino'          => $this->nb_destino,
            'Precio'          => $this->nu_precio,

        ];
    }
}
