<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Viaje as ViajeResource;
use App\Http\Requests\ViajeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Viaje;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Throwable;


class ViajeApiController extends Controller
{

    public function obtenerViajes(Viaje $viaje)
    {
        $viaje = Viaje::all();


        return response()->json([
            'Message' => 'Message Here...',
            'Status' => 200,
            'Result' => ['Viajes' => $viaje],
        ]);

    }

    public function agregarViaje(Request $request)
    {

        Viaje::create(
            array_merge([
                'co_viaje' => $request->co_viaje,
                'nu_plazas'   => $request->nu_plazas,
                'nb_origen'    => $request->nb_origen,
                'nb_destino'       => $request->nb_destino,
                'nu_precio'       => $request->nu_precio,
            ]));

        return response()->json([
            'Message' => 'Successful registration',
            'Status' => 200,
        ]);
    }

    public function actualizarViaje(Request $request, Viaje $viaje)
    {

        $viaje = Viaje::where('id_viaje', $request->id_viaje)
                ->update(["co_viaje" => $request->co_viaje,
                "nu_plazas" => $request->nu_plazas,
                "nb_origen" => $request->nb_origen,
                "nb_destino" => $request->nb_destino,
                "nu_precio" => $request->nu_precio,]);

        return response()->json([
            'Message' => 'Message Here...',
            'Status' => 200,
        ]);

    }

    public function eliminarViaje(Request $request ,Viaje $viaje)
    {


        $viaje = Viaje::where('id_viaje',$request->id_viaje);
        $viaje->delete();

        return response()->json([
            'Message' => 'Message Here...',
            'Status' => 200,
        ]);

    }

}
