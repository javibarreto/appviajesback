<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Vuelo as ViajeResource;
use App\Http\Requests\VueloRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vuelo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Throwable;


class VueloApiController extends Controller
{

    public function obtenerVuelos(Vuelo $vuelo)
    {
        $vuelo = Vuelo::all();


        return response()->json([
            'Message' => 'Message Here...',
            'Status' => 200,
            'Result' => ['Vuelo' => $vuelo],
        ]);

    }

    public function agregarVuelo(Request $request)
    {

        Vuelo::create(
            array_merge([
                'nu_ci' => $request->nu_ci,
                'nb_viajero'   => $request->nb_viajero,
                'fe_nacimiento'    => $request->fe_nacimiento,
                'nb_destino'       => $request->nb_destino,
                'nu_precio'       => $request->nu_precio,
                'tx_correo' => $request->tx_correo,
                'co_viaje'   => $request->co_viaje,
                'nb_origen'    => $request->nb_origen,
                'nb_destino'       => $request->nb_destino,
                'nu_precio'       => $request->nu_precio,
            ]));

        return response()->json([
            'Message' => 'Successful registration',
            'Status' => 200,
        ]);
    }


}
