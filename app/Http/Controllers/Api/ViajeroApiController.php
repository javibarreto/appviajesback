<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Viajero as ViajeroResource;
use App\Http\Requests\ViajeroRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Viajero;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Throwable;


class ViajeroApiController extends Controller
{

    public function obtenerViajeros(Viajero $viajero)
    {
        $viajero = Viajero::all();


        return response()->json([
            'Message' => 'Message Here...',
            'Status' => 200,
            'Result' => ['Viajeros' => $viajero],
        ]);

    }

    public function agregarViajero(Request $request)
    {
        
        Viajero::create(
            array_merge([
                'nu_ci' => $request->nu_ci,
                'nb_viajero'   => $request->nb_viajero,
                'fe_nacimiento'    => $request->fe_nacimiento,
                'nu_telefono'       => $request->nu_telefono,
                'tx_correo'       => $request->tx_correo,
            ]));

        return response()->json([
            'Message' => 'Successful registration',
            'Status' => 200,
        ]);
    }

    public function actualizarViajero(Request $request, Viajero $viajero)
    {
        $viajero = Viajero::where('id_viajero', $request->id_viajero)
                ->update(['nu_ci' => $request->nu_ci,
                'nb_viajero'   => $request->nb_viajero,
                'fe_nacimiento'    => $request->fe_nacimiento,
                'nu_telefono'       => $request->nu_telefono,
                'tx_correo'       => $request->tx_correo,]);

        return response()->json([
            'Message' => 'Message Here...',
            'Status' => 200,
        ]);

    }

    public function eliminarViajero(Request $request ,Viajero $viajero)
    {


        $viajero = Viajero::where('id_viajero',$request->id_viajero);
        $viajero->delete();

        return response()->json([
            'Message' => 'Message Here...',
            'Status' => 200,
        ]);

    }

}
