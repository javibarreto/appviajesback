<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
         $user = User::paginate(5);
         return view('user.index',compact('user'));
    }

    public function create()
    {
        $rol = Role::select('id','name')->get();
        return view('user.create', compact('rol'));
    }

    //public function store(Request $request)
    public function store(User $request)
    {
        $input = $request->all();

        dd($input);
        die();

        User::create($input);
        return redirect()->route('user.index')->with('success', 'user created successfully.');
    }

    public function show(User $user)
    {
       // $user->with(['categories', 'stores']);
        return view('user.show', compact('user'));
    }


    public function edit(User $user)
    {
        $rol = Role::select('id','name')->get();
        return view('user.edit', compact('rol'));
    }

    public function update(UserRequest $request, User $user)
    {
        $user->firtsname = $request->firtsname;
        $user->lastsname = $request->lastsname;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->phone_number = $request->phone_number;
        $user->role_id = $request->role_id;
        $user->save();

        return redirect()->route('user.index')->with('success', 'user updated successfully.');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('user.index')->with('success', 'Successful removal.');
    }

}
