<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{

    public function index()
    {
        $category = Category::paginate(5);

        return view('category.index', compact('category'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(CategoryRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('imagename')) {

        $input['imagename'] = $request->imagename->store('img_category', 'public');

        }

        Category::create($input);

        return redirect()->route('category.index')->with('success', 'Category created successfully.');

    }

    public function show(Category $category)
    {
        return view('category.show', compact('category'));
    }

    public function edit(Category $category)
    {
        return view('category.edit', compact('category'));
    }

    public function update(CategoryRequest $request, Category $category)
    {

        if ($request->hasFile('imagename')) {

            Storage::delete($category->imagename);

            $category->imagename = $request->imagename->store('img_category', 'public');
        }

        $category->name = $request->name;

        $category->save();

        return redirect()->route('category.index')->with('success', 'Category updated successfully.');
    }

    public function destroy(Category $category)
    {
        Storage::delete($category->imagename);

        $category->delete();

        return redirect()->route('category.index')->with('success', 'Successful removal.');
    }

}
