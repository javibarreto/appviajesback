<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductCollection;
use Illuminate\Support\Facades\Storage;
use App\Models\Product;
use App\Models\Category;
use App\Models\Store;

class ProductController extends Controller
{

    public function index()
    {
       $product = Product::with(['categories', 'stores'])->paginate(5);

       return view('product.index', compact('product'));
    }

    public function create()
    {
        $category = Category::select('id','name')->get();
        $store = Store::select('id','name')->get();

        return view('product.create', compact('category','store'));
    }

    public function edit(Product $product)
    {
        $category = Category::select('id','name')->get();
        $store = Store::select('id','name')->get();

        return view('product.edit', compact('product', 'category', 'store'));
    }

    public function store(ProductRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('imagename')) {

        $input['imagename'] = $request->imagename->store('img_product', 'public');

        }

        Product::create($input);

        return redirect()->route('product.index')->with('success', 'Product created successfully.');
    }

    public function show(Product $product)
    {
        $product->with(['categories', 'stores']);
        
        return view('product.show', compact('product'));
           
    }

    public function update(ProductRequest $request, Product $product)
    {
        if ($request->hasFile('imagename')) {
            
            Storage::delete($product->imagename);

            $product->imagename = $request->imagename->store('img_product', 'public');
        }

        $product->name = $request->name;
        $product->category_id = $request->category_id;
        $product->store_id = $request->store_id;
        $product->price = $request->price;
        $product->calories = $request->calories;
        $product->remarks = $request->remarks;
        $product->weight = $request->weight;
        $product->isonlycompliment = $request->isonlycompliment;
        $product->status = $request->status;
       
        $product->save();

        return redirect()->route('product.index')->with('success', 'Product updated successfully.');
    }

    public function destroy(Product $product)
    {
        Storage::delete($product->imagename);
        
        $product->delete();

        return redirect()->route('product.index')->with('success', 'Successful removal.');
    }

}
