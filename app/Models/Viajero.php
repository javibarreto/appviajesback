<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Viajero extends Model
{
//	use SoftDeletes;

    protected $table = 'viajero';
    protected $fillable = [
        'nu_ci',
        'id_viajero',
        'nb_viajero',
        'fe_nacimiento',
        'nu_telefono',
        'tx_correo',
        'created_at',
        'updated_at',
        'deleted_at'
	];
}


