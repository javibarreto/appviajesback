<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Viaje extends Model
{
//	use SoftDeletes;

    protected $table = 'viaje';
    protected $fillable = [
        'id_viaje',
        'co_viaje',
        'nu_plazas',
        'nb_origen',
        'nb_destino',
        'nu_precio',
        'created_at',
        'updated_at',
        'deleted_at'
	];
}
