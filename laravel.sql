-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-03-2021 a las 13:54:43
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `addresses`
--

CREATE TABLE `addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldstreetaddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floorno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `houseno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isdefault` tinyint(1) DEFAULT NULL,
  `buildingtype` enum('Departamento','Casa','Oficina') COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `latitude`, `longitude`, `name`, `fieldstreetaddress`, `floorno`, `houseno`, `zipcode`, `city`, `state`, `isdefault`, `buildingtype`, `district`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, -18.798689, -98.933316, 'McDermott Oval', '824 Rippin Expressway Apt. 310', '12', '227', '51884', 'East Norenefurt', 'Arizona', 0, 'Casa', 'West', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(2, 2, 59.600851, -4.707954, 'Macejkovic Springs', '6139 Marvin Mountain', '9', '721', '67941', 'Kossbury', 'Kentucky', 1, 'Departamento', 'South', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(3, 1, 87.16529, -176.205614, 'Lawrence Ridge', '51705 Kenyatta Vista Apt. 697', '22', '237', '32076', 'Isomfort', 'Rhode Island', 0, 'Casa', 'South', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(4, 2, -22.771038, 61.340368, 'Bergnaum Rest', '6347 Rylan Shoal Suite 849', '10', '197', '90937-2889', 'Everardoport', 'South Carolina', 0, 'Oficina', 'Lake', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(5, 2, -5.910102, 42.773172, 'Lois Garden', '775 Bahringer Locks', '20', '365', '80620-7984', 'North Bettie', 'South Dakota', 1, 'Oficina', 'North', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(6, 2, 24.73086, 82.856809, 'Hansen Lane', '9838 Willms Harbors Suite 923', '16', '569', '32080-2408', 'Kshlerinland', 'Rhode Island', 1, 'Departamento', 'North', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(7, 3, 49.050375, -38.96249, 'Jones Mission', '8779 Bayer Court Apt. 740', '1', '705', '92556', 'West Jennyfer', 'Oklahoma', 0, 'Oficina', 'Lake', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(8, 2, -43.622665, -87.057567, 'Reilly Via', '72705 Dickens Stravenue Apt. 854', '18', '977', '61784-4956', 'New Maryjaneville', 'Nebraska', 1, 'Oficina', 'East', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(9, 2, -61.560894, -5.081352, 'Napoleon Ramp', '17445 Boyer Ferry Apt. 693', '2', '574', '25830', 'Lake Karianestad', 'Colorado', 1, 'Oficina', 'Port', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(10, 1, 10.675173, -111.741584, 'Roob Mountains', '33628 Schmeler Ferry Apt. 608', '22', '969', '30846-1353', 'Torranceshire', 'North Dakota', 0, 'Departamento', 'Lake', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(11, 2, 21.819351, 90.384616, 'Raynor Valleys', '15800 Harris Common Apt. 681', '7', '489', '57792-0271', 'South Carterton', 'Texas', 0, 'Oficina', 'New', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(12, 3, -59.525865, -41.904474, 'Carroll Way', '82534 Batz Via Suite 618', '21', '105', '16390', 'West Uliceston', 'Utah', 1, 'Oficina', 'North', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(13, 3, 61.720065, 48.597249, 'Luna Wall', '512 Wolff Manor Suite 199', '16', '88', '64223-1036', 'Parkershire', 'Oklahoma', 0, 'Casa', 'North', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(14, 1, 38.957358, -59.751997, 'Kunde Parkways', '190 Bailey Ville Apt. 994', '8', '284', '44282', 'Dietrichshire', 'Missouri', 0, 'Casa', 'Port', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(15, 1, 24.273867, 142.770144, 'Filomena Radial', '30841 Kaycee Fords', '16', '430', '27005', 'Port Johanchester', 'Rhode Island', 1, 'Departamento', 'West', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(16, 1, -30.165641, -28.105092, 'Willa Gateway', '46003 Stewart Light Suite 225', '10', '653', '14390', 'Port Jeffreyville', 'Maine', 0, 'Oficina', 'New', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(17, 1, -34.890723, 61.441361, 'Ofelia Village', '29849 Terrence Tunnel', '13', '885', '27650-5905', 'Lake Keira', 'Florida', 0, 'Departamento', 'East', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(18, 2, 44.628854, -55.097945, 'Meggie River', '9320 Kenyon Burg', '21', '275', '16229', 'North Amystad', 'New Mexico', 0, 'Departamento', 'Port', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(19, 2, 80.880344, -81.154575, 'Goldner Trail', '31025 Dietrich Station', '19', '456', '52854', 'Jeramiefort', 'Oklahoma', 1, 'Oficina', 'East', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(20, 1, 85.865226, -106.357652, 'Elvie Summit', '21900 Kris Alley', '22', '659', '40812-1563', 'Naderton', 'New York', 1, 'Casa', 'Lake', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `application_settings`
--

CREATE TABLE `application_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `androidversion` double NOT NULL,
  `iosversion` double NOT NULL,
  `isforcestopandroid` tinyint(1) NOT NULL,
  `isforcestopios` tinyint(1) NOT NULL,
  `maintenanceios` tinyint(1) NOT NULL,
  `maintenanceandroid` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `application_user_logins`
--

CREATE TABLE `application_user_logins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userid` bigint(20) UNSIGNED NOT NULL,
  `devicetoken` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `platform` enum('') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ipaddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appversion` double NOT NULL,
  `devicemanufacturer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `devicemodel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logindatetime` datetime NOT NULL,
  `logoutdatetime` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `imagename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `imagename`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'img_category/GqRqS5fVy02m3I5iFit2p2BMJ8XXoDismZ7DyT2i.jpg', 'Almuerzo', 1, '2020-12-10 20:55:12', '2021-01-13 21:25:14', NULL),
(2, 'img_category/7d945dad9d6e2c460c9db16f551d5784.jpg', 'commodi', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(3, 'img_category/', 'odit', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(4, 'img_category/', 'beatae', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(5, 'img_category/', 'reiciendis', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(6, 'img_category/', 'autem', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(7, 'img_category/', 'et', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(8, 'img_category/', 'fugiat', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(9, 'img_category/7cb5defcdfb2f483810b6b34a2c15b87.jpg', 'quas', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(10, 'img_category/', 'nam', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(11, 'img_category/62bacbcef65a207485e51246d90d1d54.jpg', 'molestiae', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(12, 'img_category/', 'necessitatibus', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(13, 'img_category/', 'dolor', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(14, 'img_category/c5cd14aa944ba014ee885aa3ea6a8ff7.jpg', 'voluptatem', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(15, 'img_category/', 'fugiat', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(16, 'img_category/', 'delectus', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(17, 'img_category/', 'ipsum', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(18, 'img_category/', 'utas', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(19, 'img_category/', 'laborum', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(20, 'img_category/', 'voluptas', 1, '2020-12-10 20:55:12', '2020-12-10 20:55:12', NULL),
(27, 'img_category/CqcqwUMRZO3mrITZeu0d.jpg', 'ccc', 1, '2020-12-25 13:50:14', '2020-12-25 13:50:14', NULL),
(28, 'img_category/tZnm1tdWsYiLPC0GINhM8x6KZVei2FVrO5OQHejS.jpg', 'sss', 1, '2020-12-25 13:58:36', '2020-12-25 13:58:36', NULL),
(29, 'img_category/7d945dad9d6e2c460c9db16f551d5784.jpg', 'kjkkljkj', 1, '2020-12-25 14:06:49', '2020-12-25 14:06:49', NULL),
(30, 'img_category/6c093e38dcbea0ee67abd8805cedfaf6.jpg', 'ñññ', 1, '2020-12-25 14:11:04', '2020-12-25 14:11:04', NULL),
(31, 'img_category/K5arBkosJaag0hmkVuBzQOaGkEjUgjx5Y5xRK9C5.jpg', 'utattt', 1, '2020-12-25 14:50:40', '2020-12-25 14:50:40', NULL),
(32, 'img_category/1.jpg', 'sssgfdg', 1, '2021-01-13 21:27:34', '2021-01-13 21:27:34', NULL),
(33, 'img_category/u48YkHBVG5DBwTU2eeG829DxQa4OuVvpBiKOlwko.jpg', 'cateprueba', 1, '2021-01-19 13:46:26', '2021-01-19 13:46:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `complemented_products`
--

CREATE TABLE `complemented_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `complement_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `complemented_products`
--

INSERT INTO `complemented_products` (`id`, `complement_id`, `product_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(51, 2, 1, '2021-01-28 17:45:56', '2021-01-28 17:45:56', NULL),
(55, 2, 1, '2021-01-29 10:07:43', '2021-01-29 10:07:43', NULL),
(56, 1, 5, NULL, NULL, NULL),
(57, 2, 1, '2021-02-01 09:15:18', '2021-02-01 09:15:18', NULL),
(58, 2, 1, '2021-02-01 09:20:52', '2021-02-01 09:20:52', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `complements`
--

CREATE TABLE `complements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `complements`
--

INSERT INTO `complements` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Salsas', '2020-12-10 21:09:27', '2021-01-29 10:05:24', NULL),
(2, 'Otras Salsas', '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(3, 'Et.', '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(4, 'At blanditiis.', '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(5, 'Est molestiae.', '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(6, 'Ea.', '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(7, 'Molestiae id.', '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(8, 'Omnis.', '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(9, 'Vel.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(10, 'Minus in.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(11, 'Voluptas quis.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(12, 'Porro eveniet.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(13, 'Suscipit amet.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(14, 'Consequatur minus.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(15, 'Ea.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(16, 'Doloribus.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(17, 'Non error.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(18, 'Unde adipisci.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(19, 'Adipisci quos.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(20, 'Est.', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(23, 'complementos', '2021-01-27 12:04:35', '2021-01-27 12:04:35', NULL),
(24, 'contornos', '2021-01-28 17:38:19', '2021-01-28 17:40:31', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discounts`
--

CREATE TABLE `discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `value` double NOT NULL,
  `discounttype` enum('flat','percentage') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `districts`
--

CREATE TABLE `districts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `province_id` bigint(20) UNSIGNED NOT NULL,
  `shippingarea_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `topic_id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `faqs`
--

INSERT INTO `faqs` (`id`, `topic_id`, `question`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 'est', 'est', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(2, 19, 'tempore', 'dolorem', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(3, 13, 'omnis', 'ut', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(4, 20, 'necessitatibus', 'eveniet', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(5, 18, 'dolor', 'voluptates', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(6, 4, 'quisquam', 'optio', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(7, 7, 'quia', 'vel', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(8, 7, 'reprehenderit', 'vero', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(9, 12, 'ratione', 'ad', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(10, 17, 'porro', 'itaque', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(11, 7, 'odit', 'veniam', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(12, 19, 'a', 'illum', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(13, 18, 'vel', 'sed', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(14, 9, 'sit', 'non', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(15, 1, 'qui', 'molestiae', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(16, 11, 'vel', 'cupiditate', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(17, 10, 'aut', 'impedit', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(18, 17, 'totam', 'voluptatem', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(19, 19, 'et', 'laboriosam', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(20, 11, 'sunt', 'praesentium', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq_topics`
--

CREATE TABLE `faq_topics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `faq_topics`
--

INSERT INTO `faq_topics` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'eum', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(2, 'tenetur', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(3, 'ea', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(4, 'alias', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(5, 'laborum', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(6, 'ut', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(7, 'quasi', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(8, 'repudiandae', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(9, 'autem', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(10, 'dignissimos', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(11, 'rerum', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(12, 'nemo', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(13, 'libero', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(14, 'ad', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(15, 'dolores', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(16, 'voluptatem', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(17, 'ullam', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(18, 'quia', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(19, 'eligendi', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(20, 'in', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2020_09_08_154238_crear_tabla_categories', 1),
(8, '2020_09_08_165041_crear_faq_topics', 1),
(9, '2020_09_08_165135_crear_faqs', 1),
(10, '2020_09_08_171208_crear_tabla_application_settings', 1),
(11, '2020_09_08_171804_crear_tabla_shipping_areas', 1),
(12, '2020_09_08_171807_crear_tabla_departments', 1),
(13, '2020_09_08_171820_crear_tabla_provinces', 1),
(14, '2020_09_08_171833_crear_tabla_districts', 1),
(15, '2020_09_08_173618_crear_tabla_complements', 1),
(16, '2020_09_08_175543_crear_tabla_restaurants', 1),
(17, '2020_09_08_175727_crear_tabla_stores', 1),
(18, '2020_09_08_180111_crear_tabla_roles', 1),
(19, '2020_09_08_180338_crear_tabla_storetimings', 1),
(20, '2020_09_08_181253_crear_tabla_users', 1),
(21, '2020_09_08_182528_crear_tabla_applicationuserlogins', 1),
(22, '2020_09_08_201044_crear_tabla_addresses', 1),
(23, '2020_09_08_202145_crear_tabla_orders', 1),
(24, '2020_09_08_203827_crear_tabla_products', 1),
(25, '2020_09_08_204040_crear_tabla_productcomplements', 1),
(26, '2020_09_08_205450_crear_tabla_orderproducts', 1),
(27, '2020_09_08_210605_crear_tabla_orderproductcomplements', 1),
(28, '2020_09_08_221226_crear_tabla_discounts', 1),
(29, '2020_09_08_221525_crear_tabla_offers', 1),
(30, '2020_09_08_222015_crear_tabla_complementedproducts', 1),
(31, '2020_09_08_223317_crear_tabla_notifications', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `notificationtype` enum('') COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdtime` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('070710726c5dbe71b912c8482791d1729f73a046ea1f45acb08e0c3c8818de9ef255c0dda41216e8', 1, 1, 'Laravel', '[]', 0, '2020-12-18 12:30:20', '2020-12-18 12:30:20', '2021-12-18 13:30:20'),
('353d4385fdbdfb3a01a4079b18e89b099643fde8efe6d7a75f508cc55404a26c41f6cfe7dc00b1ea', 1, 1, 'Laravel', '[]', 0, '2021-01-13 15:03:09', '2021-01-13 15:03:09', '2022-01-13 16:03:09'),
('35faa85e75864c109a77e41d072a8a0049208dfe8a4e06d54fa21f7bab0a4a92dfd0dbb6c9c7a1df', 1, 1, 'Laravel', '[]', 0, '2020-12-18 12:50:34', '2020-12-18 12:50:34', '2021-12-18 13:50:34'),
('3e95226cde382b6951d1a77afb513308afcb731fb23b0c94991230d3bc3861a1739fcf277ec8deba', 1, 1, 'Laravel', '[]', 0, '2021-01-20 01:30:10', '2021-01-20 01:30:10', '2022-01-20 02:30:10'),
('4e62e4ded1a902881113a3030e8aaf4ca16749c4f2499889e251d534f0c154222cfaff96fbceb333', 1, 1, 'Laravel', '[]', 0, '2020-12-18 13:00:30', '2020-12-18 13:00:30', '2021-12-18 14:00:30'),
('5b59667ad2adfcd65d004e741f857f188a0371f7c9e0cb2c2e1b7dfda381bd26afd706ab61fa25e0', 1, 1, 'Laravel', '[]', 0, '2021-01-20 07:42:05', '2021-01-20 07:42:05', '2022-01-20 08:42:05'),
('9052aec02c0537a2f7d9c99862d13d3587d1313015d241e447025c1605f2a3c867bf92b68ef6ca2c', 1, 1, 'Laravel', '[]', 0, '2021-03-02 20:08:23', '2021-03-02 20:08:23', '2022-03-02 21:08:23'),
('97cbdca5380ff53dc70da6dae7064a693768708ef320113d31f6b179abd3ab80bfcf8527046c414f', 103, 1, 'Laravel', '[]', 0, '2021-01-20 07:42:39', '2021-01-20 07:42:39', '2022-01-20 08:42:39'),
('c53f2d4b1c2cebd26dd22537e1eaf3c7a624939673aa668022ae90edd5042e8d41bff8433b7f05a3', 1, 1, 'Laravel', '[]', 0, '2021-01-11 14:06:17', '2021-01-11 14:06:17', '2022-01-11 15:06:17'),
('cd280a051ee8514ad4db0d401d693332502f732e4606152415202a6329d6c720084ce7552484f21e', 102, 1, 'Laravel', '[]', 0, '2021-01-20 01:33:06', '2021-01-20 01:33:06', '2022-01-20 02:33:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 's9TKoBl3zZU2tsR6pucefCAYLwjQpS4PG4psJqkc', NULL, 'http://localhost', 1, 0, 0, '2020-12-18 12:29:55', '2020-12-18 12:29:55'),
(2, NULL, 'Laravel Password Grant Client', 'dNkAGfSBgWkjZtytJox6jku7rG97fspM1gQmefv4', 'users', 'http://localhost', 0, 1, 0, '2020-12-18 12:29:55', '2020-12-18 12:29:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-12-18 12:29:55', '2020-12-18 12:29:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `startdatetime` datetime NOT NULL,
  `enddatetime` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `offers`
--

INSERT INTO `offers` (`id`, `product_id`, `title`, `startdatetime`, `enddatetime`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 'repellendus', '1971-09-30 14:09:59', '2021-01-07 21:15:05', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(2, 10, 'nulla', '1978-03-12 06:16:29', '2021-01-08 23:13:06', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(3, 19, 'iure', '2009-01-15 04:14:01', '2021-01-10 13:27:25', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(4, 6, 'facere', '1994-01-23 11:51:23', '2020-12-31 11:31:42', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(5, 1, 'rerum', '2008-07-15 13:59:10', '2020-12-13 17:14:32', 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(6, 5, 'veritatis', '2004-12-10 18:04:43', '2021-01-05 08:59:20', 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(7, 19, 'voluptatem', '1996-03-23 19:23:14', '2020-12-16 02:45:58', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(8, 18, 'fugiat', '1992-04-02 18:08:05', '2021-01-04 15:34:27', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(9, 7, 'maxime', '2004-01-19 06:50:39', '2021-01-02 17:02:02', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(10, 4, 'doloremque', '2002-01-21 08:03:19', '2020-12-21 20:56:52', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(11, 19, 'rem', '2012-05-04 00:24:22', '2021-01-09 17:13:20', 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(12, 5, 'ullam', '1975-06-12 03:18:05', '2020-12-19 05:47:10', 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(13, 14, 'sit', '1976-10-26 20:12:16', '2020-12-16 19:20:33', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(14, 5, 'cumque', '1978-06-09 06:43:23', '2020-12-11 12:23:37', 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(15, 4, 'nisi', '1993-05-24 13:32:58', '2021-01-05 09:11:14', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(16, 6, 'eos', '1976-06-11 17:33:58', '2021-01-02 14:07:04', 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(17, 16, 'et', '1994-11-05 13:34:39', '2020-12-30 18:31:08', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(18, 6, 'corrupti', '1970-03-01 21:03:12', '2020-12-31 21:24:09', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(19, 7, 'et', '2004-10-14 08:19:40', '2021-01-03 01:26:06', 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(20, 12, 'sequi', '2001-05-15 09:08:57', '2020-12-14 22:09:55', 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `address_id` bigint(20) UNSIGNED NOT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `orderstatus` enum('cart','placed','shipping','delivered','cancelled','refund') COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordertype` enum('home','pickup') COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentmethod` enum('cash','card') COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderdate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `address_id`, `starttime`, `endtime`, `store_id`, `orderstatus`, `ordertype`, `paymentmethod`, `orderdate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 18, '23:28:27', '01:56:17', 19, 'cart', 'pickup', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(2, 2, 16, '23:13:05', '22:51:35', 2, 'refund', 'pickup', 'card', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(3, 1, 15, '06:19:33', '07:47:08', 4, 'cancelled', 'home', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(4, 2, 17, '11:44:43', '03:03:31', 17, 'placed', 'pickup', 'card', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(5, 3, 10, '17:50:53', '23:26:03', 14, 'shipping', 'pickup', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(6, 2, 7, '15:25:13', '18:17:21', 10, 'placed', 'home', 'card', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(7, 2, 11, '22:55:26', '12:24:05', 19, 'placed', 'pickup', 'card', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(8, 2, 12, '02:01:56', '17:31:07', 3, 'cart', 'pickup', 'card', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(9, 2, 20, '17:03:11', '21:24:15', 17, 'cancelled', 'home', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(10, 3, 20, '04:42:33', '02:58:02', 10, 'cart', 'pickup', 'card', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(11, 1, 9, '19:00:10', NULL, 12, 'cancelled', 'pickup', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(12, 3, 13, '17:02:30', '02:14:57', 11, 'delivered', 'home', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(13, 3, 7, '16:26:50', '02:18:40', 13, 'placed', 'pickup', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(14, 1, 7, '12:55:29', '02:04:25', 17, 'placed', 'pickup', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(15, 1, 16, '00:08:19', '13:53:59', 2, 'shipping', 'home', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(16, 2, 16, '15:53:26', '19:36:56', 13, 'refund', 'pickup', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(17, 1, 17, '15:05:38', '00:18:07', 12, 'cart', 'pickup', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(18, 2, 2, '04:19:20', '04:50:23', 7, 'shipping', 'pickup', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(19, 2, 20, '13:03:56', '02:56:20', 9, 'cart', 'home', 'cash', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(20, 1, 7, '19:28:07', '12:32:36', 11, 'refund', 'pickup', 'card', '2020-12-10 22:09:28', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(35, 1, 7, NULL, NULL, NULL, 'cart', 'home', 'cash', NULL, '2021-01-20 07:59:36', '2021-01-20 07:59:36', NULL),
(36, 1, 7, NULL, NULL, NULL, 'cart', 'home', 'cash', NULL, '2021-01-20 08:03:15', '2021-01-20 08:03:15', NULL),
(37, 1, 7, NULL, NULL, NULL, 'cart', 'home', 'cash', NULL, '2021-01-20 09:46:45', '2021-01-20 09:46:45', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_products`
--

CREATE TABLE `order_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `delivery` double NOT NULL,
  `total` double NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order_products`
--

INSERT INTO `order_products` (`id`, `product_id`, `quantity`, `price`, `delivery`, `total`, `order_id`, `comment`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 12, 24, 116, 13, 476, 17, 'a', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(2, 13, 87, 116, 14, 116, 15, 'modi', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(3, 13, 50, 197, 9, 100, 17, 'adipisci', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(4, 19, 66, 10, 7, 477, 16, 'aliquam', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(5, 8, 90, 131, 17, 410, 15, 'ipsum', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(6, 2, 5, 137, 9, 392, 9, 'et', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(7, 20, 80, 192, 19, 136, 8, 'ut', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(8, 14, 95, 76, 19, 265, 1, 'nam', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(9, 12, 99, 191, 16, 239, 15, 'praesentium', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(10, 10, 97, 129, 12, 430, 9, 'fuga', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(11, 7, 94, 115, 15, 306, 5, 'quae', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(12, 10, 97, 188, 8, 88, 10, 'soluta', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(13, 4, 52, 18, 11, 372, 18, 'est', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(14, 19, 36, 77, 16, 266, 1, 'quidem', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(15, 11, 59, 192, 6, 424, 17, 'accusantium', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(16, 11, 68, 107, 20, 53, 15, 'maxime', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(17, 19, 61, 11, 8, 345, 1, 'eaque', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(18, 12, 49, 195, 12, 134, 11, 'aut', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(19, 13, 39, 194, 16, 244, 4, 'incidunt', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL),
(20, 4, 43, 200, 15, 52, 1, 'qui', '2020-12-10 21:09:28', '2020-12-10 21:09:28', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_product_complements`
--

CREATE TABLE `order_product_complements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `orderproduct_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `price` double NOT NULL,
  `calories` int(11) NOT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` double NOT NULL,
  `isonlycompliment` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `imagename`, `category_id`, `store_id`, `price`, `calories`, `remarks`, `weight`, `isonlycompliment`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'pollo', 'img_product/0e872553bfce7d595069c4412c6adf10.jpg', 1, 6, 118, 191, 'Sit explicabo.', 274, 0, 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(2, 'Ea fugiat.', 'img_product/7d945dad9d6e2c460c9db16f551d5784', 3, 5, 210, 128, 'Ducimus consequatur ab.', 293, 0, 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(3, 'Recusandae.', 'img_product/', 17, 1, 289, 255, 'Quia reiciendis.', 270, 1, 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(4, 'ketchup', 'img_product/', 5, 12, 445, 180, 'Voluptatem voluptatem dolorem.', 237, 1, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(5, 'mayonesa', 'img_product/YvntRh6t7eghgH9pSIyW0EHJP9fCoue3Kd3R9z4b.jpg', 10, 10, 333, 79, 'Nihil unde voluptatem.', 78, 1, 1, '2020-12-10 21:09:27', '2021-01-26 09:24:34', NULL),
(6, 'Salsa Tartara', 'img_product/', 9, 13, 431, 247, 'Id autem reprehenderit.', 157, 1, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(7, 'Salsa Barbecue', 'img_product/', 14, 1, 169, 252, 'Qui quia laudantium.', 283, 1, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(8, 'Nobis.', 'img_product/7d945dad9d6e2c460c9db16f551d5784.png', 18, 10, 16, 293, 'Quibusdam cumque.', 126, 0, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(9, 'Quia.', 'img_product/', 7, 16, 436, 157, 'Velit cum vel.', 132, 0, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(10, 'Eaque explicabo.', 'img_product/', 3, 9, 182, 210, 'Aut eveniet.', 113, 1, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(11, 'Tempora.', 'img_product/', 2, 2, 173, 113, 'Neque soluta.', 93, 0, 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(12, 'Alias.', 'img_product/', 14, 10, 365, 195, 'Tempora incidunt.', 239, 1, 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(13, 'Aliquid doloribus.', 'img_product/', 17, 19, 254, 256, 'Ipsa nam.', 171, 1, 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(14, 'Ratione assumenda.', 'img_product/', 3, 12, 212, 223, 'Adipisci atque.', 175, 1, 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(15, 'Dolores odio.', 'img_product/', 7, 15, 455, 218, 'Ipsa tempore nemo.', 238, 0, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(16, 'Aliquam nemo.', 'img_product/', 15, 4, 385, 52, 'Laudantium atque.', 208, 1, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(17, 'Laborum eius.', 'img_product/', 20, 2, 8, 171, 'Quo distinctio illum.', 296, 0, 0, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(18, 'Accusamus.', 'img_product/', 2, 18, 340, 86, 'Iusto cum beatae.', 91, 0, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(19, 'Corporis.', 'img_product/', 13, 3, 484, 259, 'Repellendus praesentium.', 161, 0, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(20, 'Dolores.', 'img_product/', 2, 3, 357, 276, 'Quaerat consequatur laborum.', 58, 0, 1, '2020-12-10 21:09:27', '2020-12-10 21:09:27', NULL),
(26, 'productoprueba', 'img_product/en6msTYNHAJrfzlj4GTEqG4YsBaQWINi4JmOtYzj.jpg', 1, 1, 3, 1, 'sdf', 1, 1, 1, '2021-01-19 13:40:54', '2021-01-19 13:44:58', '2021-01-19 13:44:58'),
(27, 'productoprueba', 'img_product/dqH8NEYtg1hWQOvZuUmVVZ4x7BuauSZqLL5xQk2U.jpg', 1, 1, 1, 11, '1', 1, 1, 1, '2021-01-19 13:45:39', '2021-01-19 13:45:39', NULL),
(28, 'no', 'img_product/EkPzagIynPX3CG0TiEnmFVr09bKOYZ6FerPf7uUr.jpg', 2, 1, 1, 2, 'sdf', 1, 1, 1, '2021-01-24 20:14:35', '2021-01-24 20:14:35', NULL),
(29, 'complemento', 'img_product/YpRsN11LuB6sHjQJ1sr4OveYjJZanR7l7doDAaBx.jpg', 2, 1, 2, 2, '1', 1, 1, 1, '2021-01-24 20:16:26', '2021-01-24 20:16:26', NULL),
(30, 'cp', 'img_product/8tIZKwH95uLP0gsuuvifL3WfPgcnyZF5hGrcKd4f.jpg', 2, 1, 1, 1, 'sdf', 1, 0, 1, '2021-01-24 20:18:13', '2021-01-24 20:18:13', NULL),
(31, 'm', 'img_product/qJtD3G2GtQS17aSKT1y1BTZVS5OGXfc6j8U6eDHc.jpg', 5, 3, 1, 1, 'sdf', 2, 1, 1, '2021-01-24 20:20:39', '2021-03-02 18:28:08', '2021-03-02 18:28:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_complements`
--

CREATE TABLE `product_complements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `complement_id` bigint(20) UNSIGNED NOT NULL,
  `ismendatory` tinyint(1) NOT NULL,
  `multiselect` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `product_complements`
--

INSERT INTO `product_complements` (`id`, `product_id`, `complement_id`, `ismendatory`, `multiselect`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, 1, 0, '2020-12-10 21:09:28', '2021-01-28 11:42:09', NULL),
(4, 7, 2, 0, 1, '2020-12-10 21:09:28', '2021-01-28 11:01:35', '2021-01-28 11:01:35'),
(24, 6, 2, 1, 1, '2021-01-28 12:15:21', '2021-01-28 12:16:39', '2021-01-28 12:16:39'),
(26, 5, 1, 1, 1, '2021-01-28 17:41:18', '2021-01-28 17:41:18', NULL),
(28, 4, 7, 1, 1, '2021-01-29 11:17:08', '2021-01-29 11:17:08', NULL),
(29, 4, 5, 1, 1, '2021-01-29 11:20:39', '2021-01-29 11:20:39', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provinces`
--

CREATE TABLE `provinces` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `department_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurants`
--

CREATE TABLE `restaurants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactnumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentmethod` enum('onlycash','onlycard','all') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `restaurants`
--

INSERT INTO `restaurants` (`id`, `name`, `website`, `logo`, `contactnumber`, `paymentmethod`, `email`, `phonenumber`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Restaurant Test 1', 'www.test1.com', 'Test Logo 1', '999999999', 'onlycash', 'test1@ejemplo.com', '255255', NULL, NULL, NULL),
(2, 'Restaurant Test 2', 'www.test2.com', 'Test Logo 2', '999999999', 'onlycash', 'test2@ejemplo.com', '255255', NULL, NULL, NULL),
(3, 'Restaurant Test 3', 'www.test1.com', 'Test Logo 3', '999999999', 'onlycash', 'test3@ejemplo.com', '255255', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administrator', '2020-12-10 20:39:21', '2020-12-10 20:39:21', NULL),
(2, 'Client', '2020-12-10 20:39:21', '2020-12-10 20:39:21', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shipping_areas`
--

CREATE TABLE `shipping_areas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stores`
--

CREATE TABLE `stores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `restaurant_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `streetaddress` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admincontact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `stores`
--

INSERT INTO `stores` (`id`, `restaurant_id`, `name`, `address`, `streetaddress`, `latitude`, `longitude`, `zipcode`, `city`, `state`, `phonenumber`, `email`, `admincontact`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'laborum', '670 Beatty Key Suite 179\nWest Maxwell, HI 08636', '17380 Wilderman Plain', 48.639065, -104.089219, '63611-3110', 'Paulineshire', 'Maine', '+1 (554) 507-2989', 'gerson58@gmail.com', 'Carissa Kassulke', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(2, 1, 'impedit', '48392 Marta Trace\nEast Jayne, AR 69127', '41177 Corkery Mountain Apt. 667', 60.883786, 57.704879, '00960', 'New Mike', 'California', '+1-401-572-8998', 'koss.edwin@hotmail.com', 'Dr. Gregoria Nicolas', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(3, 3, 'praesentium', '225 Noah Roads Suite 331\nEast Monte, IL 55895-6639', '3072 Rath Burg', 28.214424, -16.870759, '90739', 'Lake Trystanfort', 'Oregon', '392.659.3956 x6110', 'jarred.mraz@gmail.com', 'Dr. Enid DuBuque MD', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(4, 1, 'sunt', '47495 Norene Forest Apt. 136\nErwinville, IN 20708-5210', '62521 Swaniawski Crossing Apt. 476', -29.877645, 27.092649, '20099-5672', 'Lucioustown', 'Maine', '1-452-835-2239', 'domingo.buckridge@hotmail.com', 'Ms. Mertie Stamm', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(5, 3, 'rem', '9288 Padberg Gateway Apt. 336\nEmmittfort, VA 87438', '3660 Florian Road Suite 695', 16.292849, -75.523197, '95039-9126', 'Kerlukeberg', 'Vermont', '1-875-797-9833 x1747', 'kacey83@brakus.com', 'Mr. Ismael McCullough', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(6, 2, 'animi', '22601 Albert Flat\nGabestad, MA 14160', '3556 Arden Loaf Apt. 639', 35.984711, 102.54992, '27197-4129', 'Arichaven', 'Arkansas', '1-983-998-9924', 'oferry@dicki.com', 'Margarete Crist', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(7, 1, 'odio', '8284 Effertz Forges\nNew Godfreyville, CT 70333-7611', '5834 Muhammad Gateway Suite 747', 11.069619, 99.078819, '18966', 'Lake Domenickville', 'Connecticut', '+1.695.217.6167', 'zgulgowski@hotmail.com', 'Hilbert Bergnaum', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(8, 1, 'beatae', '535 Kuhn Lodge\nRogerchester, NY 14002-3093', '2287 Dean Summit Apt. 602', -64.842679, 75.274807, '21284', 'Zettatown', 'Ohio', '+1.384.753.8001', 'hope61@breitenberg.com', 'Mr. Adolph Hill', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(9, 1, 'aut', '698 Goldner Port\nSouth Susanna, KY 36418-6429', '706 Norberto Curve', -0.851826, 45.429624, '05471-4651', 'Aidanville', 'Oregon', '1-679-774-9718 x0741', 'glang@batz.info', 'Prof. Giovani Mosciski V', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(10, 1, 'delectus', '1672 Hartmann Streets\nBlairport, IN 92687', '554 Runte Light', 45.344664, -46.039596, '82018', 'Giannistad', 'Maine', '1-539-545-7754 x5823', 'bashirian.kaitlyn@borer.info', 'Myrtice Klein', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(11, 2, 'doloremque', '53638 Homenick Stravenue Apt. 644\nLednertown, NY 84739-1117', '451 Cierra Crossroad Apt. 683', -10.238653, -50.152396, '64024', 'Port Rosamondville', 'Utah', '(428) 534-3519 x77072', 'cristobal88@yahoo.com', 'Luciano Conroy', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(12, 2, 'labore', '413 Yost Lodge\nBogisichmouth, NH 59506', '37846 Cremin Drive', 1.981741, -76.113832, '69727-4573', 'Jermaineside', 'Massachusetts', '809-229-7054 x684', 'adrian.rice@gmail.com', 'Will Fahey', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(13, 3, 'laudantium', '18990 Mertz Burg\nNorth Ernestofurt, LA 50329-8984', '927 Graham Junctions', 76.730895, 117.764463, '56826', 'South Erika', 'Texas', '1-661-201-3862 x4500', 'veum.tianna@rohan.info', 'Noemy Funk', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(14, 3, 'qui', '4624 Odell Streets Suite 668\nPort Melvina, NH 13047-3931', '36937 Altenwerth Tunnel', 75.203223, 116.541447, '63646-8890', 'Port Milan', 'Oklahoma', '+1-973-448-0496', 'vwaelchi@gmail.com', 'Amber Schmitt III', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(15, 1, 'sint', '471 Ledner Union Suite 639\nPort Alfredoshire, UT 83681', '7228 Wilma Fork', -71.985702, 106.335217, '56048-8248', 'Schroedershire', 'North Carolina', '(841) 893-8672', 'kjones@yahoo.com', 'Jaqueline Smitham', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(16, 1, 'optio', '70120 Damien Stream\nCormiermouth, NM 82350', '53998 Ron Wells Suite 457', 26.750193, 166.753397, '39344', 'East Reybury', 'Kansas', '668.590.0313 x022', 'zrolfson@hills.com', 'Andres Schuppe V', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(17, 1, 'nihil', '9444 Augusta Flat Suite 056\nSouth Newell, MA 16219-5485', '1679 Hiram Lock Apt. 174', -54.902838, 93.561082, '41370-6190', 'North Lysannehaven', 'Montana', '(882) 309-8407 x532', 'alva99@wyman.biz', 'Sally Reilly', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(18, 1, 'non', '9893 Reichert Village Suite 054\nSkylahaven, IN 56537-1603', '1975 Corkery Drives', -52.358903, -97.819001, '94694', 'Port Darrion', 'Alabama', '1-821-417-6829 x1613', 'dschulist@hotmail.com', 'Mr. Isaiah Maggio II', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(19, 1, 'aut', '4935 Weissnat Brook Suite 537\nOlsonchester, OR 08967-2378', '350 Joy Rue', -20.784698, 44.936961, '28914', 'East Samir', 'Arkansas', '924-945-3941 x61960', 'nitzsche.bailey@hotmail.com', 'Fritz Gulgowski V', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL),
(20, 3, 'earum', '72908 Johnnie Burg Suite 825\nSwaniawskimouth, NM 80564', '39146 Hirthe Crossing', 75.167815, 44.859913, '59522', 'Milesfurt', 'California', '+1-351-401-8221', 'croob@hotmail.com', 'Prof. Jayce Hamill', '2020-12-10 20:39:22', '2020-12-10 20:39:22', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `store_timings`
--

CREATE TABLE `store_timings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `store_id` bigint(20) UNSIGNED NOT NULL,
  `dayofweek` enum('sun','mon','tue','wed','thu','fri','sat') COLLATE utf8mb4_unicode_ci NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profileimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dninumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT 1,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `email_verified_at`, `password`, `remember_token`, `profileimage`, `username`, `phone_number`, `dninumber`, `isactive`, `role_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administratpr', 'Admin', 'admin@yam.test', NULL, '$2y$10$tvOlqZyKq7lwSTcsgUvYtOd7MbcIws87t4WRcyE281CN/NCBmBpvi', '2EaasAvteB5ZmPuIQaj4Qj5JhAlnc4WaxYMgLgcBtqiJFSgSYRozHjGiCBIt', NULL, 'pruebuua', '999999999', 'prueba5', 1, 1, '2020-12-10 20:39:21', '2021-01-20 07:43:05', NULL),
(2, 'Client 1', 'test', 'client1@yam.test', NULL, '$2y$10$hnpDYhyUNmdDYCQw.PdGhuDTJMKoFdSGlvAklfUI7Y3.natVSllVS', 'gCKtRXItuiuufEo6AtJtvw7tfOqOQq1bzBn4S9Ym1TroDdpoBnm2XcaRDN7C', NULL, NULL, '999999991', NULL, 1, 2, '2020-12-10 20:39:21', '2020-12-10 20:39:21', NULL),
(3, 'Client 2', 'test', 'client2@yam.test', NULL, '$2y$10$6CN6UNd2aegNpUntfoA3yeTIkBKAnS3vLFRb122FdQo/NahrR6nn6', NULL, NULL, NULL, '999999994', NULL, 1, 2, '2020-12-10 20:39:21', '2020-12-24 21:19:23', NULL),
(13, 'javier', 's', 'adwmin@yam.test', NULL, '$2y$10$hnpDYhyUNmdDYCQw.PdGhuDTJMKoFdSGlvAklfUI7Y3.natVSllVS', '$2y$10$hnpDYhyUNmdDYCQw.PdGhuDTJMKoFdSGlvAklfUI7Y3.natVSllVS', NULL, 'admin@yam.test', '299999994', '4234488', 1, 1, '2020-12-24 16:59:55', '2020-12-24 21:18:18', '2020-12-24 21:18:18'),
(14, 'sss', 's', 'admi34n@yam.test', NULL, '$2y$10$3lx4ebwCQ1AqHt9RXKOBge7A8Vw6xYrTFXeJ1f29w7zq2uln6qczu', '$2y$10$0VhXFokwmbyteGK/C9eMeOjEMuyly/UgbNvyLze8S5zf5wcjzkGOG', NULL, 'ww', '444999994', '23423', 1, 1, '2020-12-24 17:51:27', '2020-12-24 21:18:17', '2020-12-24 21:18:17'),
(15, 'javierppp', 'sdf', 'a4dmin@yam.test', NULL, '$2y$10$HkzUMiHNWzq5aocwvwp6weI9bny9A.53pQB1jDHUtuW7CDO.tjzGm', NULL, NULL, 'www', '990999994', '423488', 1, 1, '2020-12-24 17:55:05', '2020-12-24 21:18:13', '2020-12-24 21:18:13'),
(16, 'q', 'q', 'javier1.barret@hotmail.com', NULL, '$2y$10$kLen0PCu1jTN9Y2dWdc9Te1uQntVUiw8YHLhsBeYThvCZ.pX.Lgu2', NULL, NULL, 'admin@yam.test', '999969994', '4234', 1, 1, '2020-12-24 18:01:54', '2020-12-24 21:18:09', '2020-12-24 21:18:09'),
(17, 'sss', 's', 'tadmin@yam.test', NULL, '$2y$10$WESSHFkey61MUdPnhX/o0Oabqmo99WYanMpX3eV9MTgXgHQlyebcK', NULL, NULL, 'admggin@yam.test', '998999994', '423488', 1, 1, '2020-12-24 18:15:00', '2020-12-24 21:18:05', '2020-12-24 21:18:05'),
(18, 'javier', 'barreto', 'admin@yam.testt', NULL, '$2y$10$ZpsbTxTq5.D3YXxo1nGvSusPoDzcgaV5GZxufZ8bclqnKEzy7VIRO', NULL, NULL, 'admin@yam.testt', '899999994', '423488', 1, 1, '2020-12-24 21:09:09', '2020-12-24 21:18:02', '2020-12-24 21:18:02'),
(19, 'javier', 'barreto', 'adsmin@yam.test', NULL, '$2y$10$5Mm5cy91H2qiRjvzCjqCOO8XjTdvjClEHbQblSq0rduSvhArTVTCe', 'NbaEmf1YaX6ZfXF1o40i2i59HiextLdIs7r72G1g', NULL, 'sdsd', '994999994', '423488', 1, 1, '2020-12-24 21:16:46', '2020-12-24 21:17:58', '2020-12-24 21:17:58'),
(20, 'ssdppp', 'barreto', 'javier.barret@hotmail.com', NULL, '$2y$10$CEMRHV67valTc7KgMpd8IuFbyfZq2g9B7Z5pHV8M/WKzty63D7p0u', 'GKrBxbt5Wl8W92q00ZzKwI5WAdGPKBHOijuS9Va3', NULL, 'javiBarreto', '999999997', '423488', 1, 1, '2020-12-25 11:59:23', '2020-12-25 12:45:50', NULL),
(101, 'sdlfkj', 'sdfd', 'sdfksdjf@tam.com', NULL, '$2y$10$9ggRztBqMovl3MejJZyZKeG5XSmWCON/CrCKMSH5szZN.pUDosMIy', NULL, NULL, NULL, '644545434', NULL, 1, 2, '2021-01-20 01:28:44', '2021-01-20 01:28:44', NULL),
(102, 'uuuuu', 'uuuuu', 'uuuuuj@tam.com', NULL, '$2y$10$o/dwxrV9UcdBgfE9/QPKd.v4Imn8qA6imG2fYMABKfdRGXaySCA7i', NULL, NULL, 'uuuuu', '222225430', '222222222', 1, 1, '2021-01-20 01:33:06', '2021-01-25 11:04:21', NULL),
(103, 'sdlfkj', 'sdfd', 'sdfksdj@gam.com', NULL, '$2y$10$WB2zvPNJ9QA6ex7pbvJYTuNHjUnmfHZ4Oe1j9jDLB5N.6QrK2Ke3a', NULL, NULL, NULL, '644549430', NULL, 1, 2, '2021-01-20 07:42:39', '2021-01-25 11:03:46', '2021-01-25 11:03:46'),
(104, 'javier', 'barreto', 'adm77in@yam.test', NULL, '$2y$10$yJ/.0x9otlh0qmVVS0ZiUutIlkP4geBw1.0LOuqm2qh39gBYXJ3QS', 'QZOvhXYmmacNykk6z127meXHPFqnEuUPBBaMmGUS', NULL, 'admin@yam.test', '999979994', '888877778', 1, 1, '2021-01-25 11:05:02', '2021-01-25 11:05:02', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viaje`
--

CREATE TABLE `viaje` (
  `id_viaje` int(11) NOT NULL,
  `co_viaje` varchar(45) COLLATE utf8mb4_bin NOT NULL,
  `nu_plazas` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `nb_origen` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `nb_destino` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `nu_precio` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `viaje`
--

INSERT INTO `viaje` (`id_viaje`, `co_viaje`, `nu_plazas`, `nb_origen`, `nb_destino`, `nu_precio`, `created_at`, `updated_at`) VALUES
(1, '0005', '54', 'Caracas', 'Tokio', '3443', '2021-03-02 21:11:00', '2021-03-02 21:11:00'),
(2, '0002', '23', 'Caracas', 'España', '231', NULL, NULL),
(3, '0003', '44', 'Caracas', 'Miami', '1132', NULL, NULL),
(4, '0005', '54', 'Caracas', NULL, NULL, '2021-03-02 20:20:07', '2021-03-02 20:20:07'),
(5, '0005', '54', 'Caracas', 'Tokio', '3443', '2021-03-02 21:08:38', '2021-03-02 21:08:38'),
(6, '0005', '54', 'Caracas', 'Tokio', '3443', '2021-03-02 21:09:22', '2021-03-02 21:09:22'),
(7, '0005', '54', 'Caracas', 'Tokio', '3443', '2021-03-02 21:12:32', '2021-03-02 21:12:32'),
(8, '0005', '54', 'Caracas', 'Tokio', '3443', '2021-03-02 21:13:06', '2021-03-02 21:13:06'),
(9, '0005', '54', 'Caracas', NULL, NULL, '2021-03-02 22:46:42', '2021-03-02 22:46:42'),
(10, '2222', NULL, 'Caracas', 'Dubai', '2342', '2021-03-03 00:31:15', '2021-03-03 00:31:15'),
(11, '2222', NULL, 'Caracas', 'Dubai', '2342', '2021-03-03 00:36:18', '2021-03-03 00:36:18'),
(12, '2222', NULL, 'Caracas', 'Dubai', '2342', '2021-03-03 00:37:21', '2021-03-03 00:37:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viajero`
--

CREATE TABLE `viajero` (
  `id_viajero` int(11) NOT NULL,
  `nu_ci` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `nb_viajero` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `fe_nacimiento` date DEFAULT NULL,
  `nu_telefono` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `tx_correo` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `viajero`
--

INSERT INTO `viajero` (`id_viajero`, `nu_ci`, `nb_viajero`, `fe_nacimiento`, `nu_telefono`, `tx_correo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, '234234', 'sdfssssss', '1986-11-11', '34234', 'sdfsdfssss', '2021-03-02 22:52:41', '2021-03-03 00:16:20', NULL),
(3, NULL, NULL, NULL, NULL, NULL, '2021-03-02 22:53:10', '2021-03-02 22:53:10', NULL),
(4, '234234', 'sdfs', '1986-11-11', NULL, NULL, '2021-03-02 22:56:28', '2021-03-02 22:56:28', NULL),
(5, '234234', 'sdfs', '1986-11-11', NULL, NULL, '2021-03-02 22:57:49', '2021-03-02 22:57:49', NULL),
(6, '234234', 'sdfs', '1986-11-11', '34234', 'sdfsdf', '2021-03-02 23:00:00', '2021-03-02 23:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo`
--

CREATE TABLE `vuelo` (
  `id_vuelo` int(11) NOT NULL,
  `nu_ci` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `nb_viajero` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `fe_nacimiento` date DEFAULT NULL,
  `nu_telefono` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `tx_correo` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `co_viaje` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `nb_origen` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `nb_destino` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `nu_precio` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `vuelo`
--

INSERT INTO `vuelo` (`id_vuelo`, `nu_ci`, `nb_viajero`, `fe_nacimiento`, `nu_telefono`, `tx_correo`, `co_viaje`, `nb_origen`, `nb_destino`, `nu_precio`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '34234', 'javier', '1983-11-11', '423423', 'dfsdfsdf', '222', 'caracas', 'japon', 2342, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL, NULL, '2222', 'Caracas', 'Dubai', 2342, '2021-03-03 00:37:57', '2021-03-03 00:37:57', NULL),
(3, '55334', 'javeeeier', '1983-11-11', NULL, 'dsswwww', '22ee22', 'Caracas', 'India', 2342, '2021-03-03 00:39:15', '2021-03-03 00:39:15', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `application_settings`
--
ALTER TABLE `application_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `application_user_logins`
--
ALTER TABLE `application_user_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_user_logins_userid_foreign` (`userid`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `complemented_products`
--
ALTER TABLE `complemented_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complemented_products_complement_id_foreign` (`complement_id`),
  ADD KEY `complemented_products_product_id_foreign` (`product_id`);

--
-- Indices de la tabla `complements`
--
ALTER TABLE `complements`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discounts_product_id_foreign` (`product_id`);

--
-- Indices de la tabla `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `districts_province_id_foreign` (`province_id`),
  ADD KEY `districts_shippingarea_id_foreign` (`shippingarea_id`);

--
-- Indices de la tabla `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faqs_topic_id_foreign` (`topic_id`);

--
-- Indices de la tabla `faq_topics`
--
ALTER TABLE `faq_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offers_product_id_foreign` (`product_id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_address_id_foreign` (`address_id`),
  ADD KEY `orders_store_id_foreign` (`store_id`);

--
-- Indices de la tabla `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_products_product_id_foreign` (`product_id`),
  ADD KEY `order_products_order_id_foreign` (`order_id`);

--
-- Indices de la tabla `order_product_complements`
--
ALTER TABLE `order_product_complements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_product_complements_orderproduct_id_foreign` (`orderproduct_id`),
  ADD KEY `order_product_complements_product_id_foreign` (`product_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`),
  ADD KEY `products_store_id_foreign` (`store_id`);

--
-- Indices de la tabla `product_complements`
--
ALTER TABLE `product_complements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_complements_product_id_foreign` (`product_id`),
  ADD KEY `product_complements_complement_id_foreign` (`complement_id`);

--
-- Indices de la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinces_department_id_foreign` (`department_id`);

--
-- Indices de la tabla `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `shipping_areas`
--
ALTER TABLE `shipping_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stores_restaurant_id_foreign` (`restaurant_id`);

--
-- Indices de la tabla `store_timings`
--
ALTER TABLE `store_timings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_timings_store_id_foreign` (`store_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_number_unique` (`phone_number`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `viaje`
--
ALTER TABLE `viaje`
  ADD PRIMARY KEY (`id_viaje`);

--
-- Indices de la tabla `viajero`
--
ALTER TABLE `viajero`
  ADD PRIMARY KEY (`id_viajero`);

--
-- Indices de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `application_settings`
--
ALTER TABLE `application_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `application_user_logins`
--
ALTER TABLE `application_user_logins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `complemented_products`
--
ALTER TABLE `complemented_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `complements`
--
ALTER TABLE `complements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `districts`
--
ALTER TABLE `districts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `faq_topics`
--
ALTER TABLE `faq_topics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de la tabla `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `order_product_complements`
--
ALTER TABLE `order_product_complements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `product_complements`
--
ALTER TABLE `product_complements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `shipping_areas`
--
ALTER TABLE `shipping_areas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `stores`
--
ALTER TABLE `stores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `store_timings`
--
ALTER TABLE `store_timings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT de la tabla `viaje`
--
ALTER TABLE `viaje`
  MODIFY `id_viaje` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `viajero`
--
ALTER TABLE `viajero`
  MODIFY `id_viajero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  MODIFY `id_vuelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `application_user_logins`
--
ALTER TABLE `application_user_logins`
  ADD CONSTRAINT `application_user_logins_userid_foreign` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `complemented_products`
--
ALTER TABLE `complemented_products`
  ADD CONSTRAINT `complemented_products_complement_id_foreign` FOREIGN KEY (`complement_id`) REFERENCES `complements` (`id`),
  ADD CONSTRAINT `complemented_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Filtros para la tabla `discounts`
--
ALTER TABLE `discounts`
  ADD CONSTRAINT `discounts_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Filtros para la tabla `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `districts_shippingarea_id_foreign` FOREIGN KEY (`shippingarea_id`) REFERENCES `shipping_areas` (`id`);

--
-- Filtros para la tabla `faqs`
--
ALTER TABLE `faqs`
  ADD CONSTRAINT `faqs_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `faq_topics` (`id`);

--
-- Filtros para la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `offers_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  ADD CONSTRAINT `orders_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`),
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Filtros para la tabla `order_product_complements`
--
ALTER TABLE `order_product_complements`
  ADD CONSTRAINT `order_product_complements_orderproduct_id_foreign` FOREIGN KEY (`orderproduct_id`) REFERENCES `order_products` (`id`),
  ADD CONSTRAINT `order_product_complements_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `products_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`);

--
-- Filtros para la tabla `product_complements`
--
ALTER TABLE `product_complements`
  ADD CONSTRAINT `product_complements_complement_id_foreign` FOREIGN KEY (`complement_id`) REFERENCES `complements` (`id`),
  ADD CONSTRAINT `product_complements_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Filtros para la tabla `provinces`
--
ALTER TABLE `provinces`
  ADD CONSTRAINT `provinces_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`);

--
-- Filtros para la tabla `stores`
--
ALTER TABLE `stores`
  ADD CONSTRAINT `stores_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`);

--
-- Filtros para la tabla `store_timings`
--
ALTER TABLE `store_timings`
  ADD CONSTRAINT `store_timings_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
